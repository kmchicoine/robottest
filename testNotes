Here are the interfaces needed to run the robot.  The required parameters are the values that I currently have hard-coded but should probably be passable.  The data returned is what I propose a call to each sensor's function would return.
Sonar:
    Required parameters:
        -String: Trigger pin (ex: "P9_23")
        -String: Echo pin (ex: "P9_12")
    Data returned:
        -Float: distance to obstacle

Compass:
    Required parameters:
        -Int: bus number
        Note: compass works using I2C, and the Beaglebone only has one I2C interface enabled by default.  
    Data returned (currently):
        -Tuple of tuples: (accelerometer reading, magnometer reading)
            -Magnometer reading: tuple of floats which are the x/y/z axes of magnetic force
            -Accelerometer reading (optional, doesn't have specific use yet): tuple of floats which are x/y/z accelerations 
        -Proposed change: return a float which gives the heading (add math to convert from magnetic force vectors to angle)

GPS:
    Required parameters:
        -String: UART interface (ex: "UART2")
        -String: port (ex: "/dev/ttyO2")
        -Int: baudrate
    Data returned:
        -String: NMEA-formatted GPS reading

Motor Controller:
    Required parameters:
        -String: UART interface (ex: "UART1")
        -String: port (ex: "/dev/ttyO1")
        -Int: baudrate
    Data returned:
        -none


I am using some Adafruit BeagleBone Python libraries. Links to their respective GitHub repositories:
-Adafruit_BBIO: https://github.com/adafruit/adafruit-beaglebone-io-python
    -Adafruit_BBIO.UART: Used for motor controller and GPS
    -Adafruit_BBIO.GPIO: Used for sonar
-Adafruit_LSM303: https://github.com/adafruit/Adafruit_Python_LSM303
    -Used for compass/accelerometer

Additionally, I use the Python serial library for writing to the UART ports
-pyserial: https://github.com/pyserial/pyserial
