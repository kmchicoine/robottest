#!/bin/bash
#configure pins

export PRUN=0
export TARGET=pwm1

machine=$(awk '{print $NF}' /proc/device-tree/model)

echo -n $machine
echo " Found"

pin="P9_30"

echo $pin
config-pin $pin pruout
config-pin -q $pin
