import Adafruit_BBIO.UART as UART
import serial

UART.setup("UART1")
ser = serial.Serial(port = "/dev/ttyO1", baudrate=9600)
while ser.isOpen():
    val = int(input("Enter 0-255: "))
    if (val < 0 or val > 255):
        print("invalid input")
    else:
        ser.write(bytearray([val]))
