import Adafruit_BBIO.GPIO as GPIO
import time
import Adafruit_BBIO.PWM as PWM
import datetime as DT

def distMeas(TRIG, ECHO):
    GPIO.output(TRIG, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(TRIG, GPIO.LOW)
#    while GPIO.input(ECHO) == GPIO.HIGH:
#        pulseStart = DT.datetime.now()
#    while GPIO.input(ECHO) == GPIO.LOW:
#        pulseEnd = DT.datetime.now()
    edgeHigh = GPIO.wait_for_edge(ECHO, GPIO.RISING, 100)
    if edgeHigh is not None:
        pulseStart = time.time()
    else:
        return -1

    edgeLow = GPIO.wait_for_edge(ECHO, GPIO.FALLING, 100)
    if edgeLow is not None:
        pulseEnd = time.time()
    else:
        return -1

    pulseDur = pulseEnd - pulseStart
    print("pulseDur: ", pulseDur) 
    dist = pulseDur * 17150
    dist = round(dist, 2)
    return dist

GPIO.setup("P9_23", GPIO.OUT) #Trigger
GPIO.setup("P9_12", GPIO.IN) #Echo

GPIO.output("P9_23", GPIO.LOW)
time.sleep(0.5)

try:
    while True:
        print ("get distance")
        test = distMeas("P9_23", "P9_12")
        print( "Dist: ", test, "cm" )
        time.sleep(1)

except KeyboardInterrupt:
    print( "Stopped")
    GPIO.cleanup()
