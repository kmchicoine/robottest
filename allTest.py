import Adafruit_BBIO.UART as UART
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_LSM303 as LSM
import serial
import time
import datetime as DT
import threading
from threading import Thread

#set user input to 0 before start of program
USERIN = 0

#motor controller initialization
#required input arguments: 
#   -String: UART port ("UART1")
#   -String: port name ("/dev/ttyO1")
#   -Int: baudrate
UART.setup("UART1")
motor = serial.Serial(port="/dev/ttyO1", baudrate=9600)

#sonar initialization
#required input arguments:
#   -String: trigger pin ("P9_23")
#   -String: echo pin ("P9_12")

OBSTACLE = False
TRIG = "P9_23"
ECHO = "P9_12"
GPIO.setup(TRIG, GPIO.OUT) #sonar trigger
GPIO.setup(ECHO, GPIO.IN) #sonar echo
GPIO.output(TRIG, GPIO.LOW)
time.sleep(0.5)

#GPS initialization
#required input arguments:
#   -String: UART port ("UART2")
#   -String: port name ("/dev/ttyO2")
#   -Int: baudrate
UART.setup("UART2")
GPS = serial.Serial(port="/dev/ttyO2", baudrate=9600)

#compass initialization 
#required input arguments:
#   -Int: bus number
lsm = LSM.LSM303(busnum=2)

class Sonar(Thread):
    def __init__(self):
        global OBSTACLE
        Thread.__init__(self)
        self.start()
    def run(self):
        while True:
            #this is the important part of the sonar code:
            #send a pulse, time the return of the pulse, and
            #calculate the distance to the nearest obstacle
            GPIO.output(TRIG, GPIO.HIGH)
            time.sleep(0.00001)
            GPIO.output(TRIG, GPIO.LOW)
            while (GPIO.input(ECHO) == GPIO.HIGH):
                pusleStart = DT.datetime.now()
            while (GPIO.input(ECHO) == GPIO.LOW):
                pulseEnd = DT.datetime.now()
            pusleDur = pulseEnd-pulseStart
            dist = pulseDur.microseconds/58
            #end of important part. I imagine returning dist
            #when the sonar function is called in an automata?
            if (dist < 30): #distance < 30 cm
                OBSTACLE = True
                print("Detected obstacle")
            time.sleep(1)

class UserInput(Thread):
#UserInput may not be neccessary for the DRP goals, as
#the pathfinding algorithm can be predetermined. Would probably
#be nice to have, or a kill-all user input for stopping the robot
#from running into things
    def __init__(self):
        global USERIN
        Thread.__init__(self)
        self.start()
    def run(self):
        while True:
            #the motor controller uses 8 bit integers to control motors
            #0: all stop
            #1-63: motor1 reverse (1 is full speed reverse)
            #64: motor1 stop
            #65-127: motor1 forward (127 full speed forward)
            #128-191: motor2 reverse (128 full speed reverse)
            #192: motor2 stop
            #192-255: motor2 forward (255 full speed forward)
            val = input("Enter input 0-255: ")
            try:
                val = int(val)
                if val < 0 or val > 255:
                    USERIN = 0
                    print("Invalid input: out of range (0-255)")
                else:
                    USERIN = val
                time.sleep(1)
            except ValueError:
                print("Invalid input: must be int")

class GPSOut(Thread):
    def __init__(self):
        #waiting for GPS to triangulate itself could happen
        #during the GPS initialization 
        while GPS.inWaiting()==0:
            pass
        Thread.__init__(self)
        self.start()
    def run(self):
        while True:
            #pretty self explanatory: read from serial port until
            #newline is encountered
            NMEA=GPS.readline()
            print("GPS: ", NMEA)
            time.sleep(1)

class CompassOut(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.start()
    def run(self): 
        while True:
            #the lsm303.read() call returns a tuple of tuples. The first
            #tuple is the accelerometer data as a tuple of the acceleration 
            #in the x,y,z directions. The second tuple is the magnometer data,
            #also as a tuple of magnetic field intensity on the x,y,z axes. I'm 
            #thinking the call to Compass should return the heading, which is 
            #just a few lines of math to convert vectors to angles (arctan(y/x))
            accel, mag = lsm303.read()
            accelX, accelY, accelZ = accel
            magX, magY, magZ = mag
            print("accel x, y, z: {0}, {1}, {2}".format(accelX, accelY, accelZ))
            print("mag x, y, z: {0}, {1}, {2}".format(magX, magY, magZ))
            time.sleep(1)
#python sonar doesn't work
#t1 = threading.Thread(name='Sonar', target=Sonar)
#t1.start()
t2 = threading.Thread(name='UserInput', target=UserInput)
t2.start()
t3 = threading.Thread(name='GPSOut', target=GPSOut)
t3.start()
t4 = threading.Thread(name='CompassOut', target=CompassOut)
t4.start()

try: 
    while ser.isOpen():
        if OBSTACLE:
            #if obstacle detected, stop robot
            motor.write(bytearray([0]))
            print("Obstacle detected")
            break
        else:
            #if no obstacle detected, write the user's input
            #to the motor controller
            motor.write(bytearray([USERIN]))
except KeyboardInterrupt:
    GPIO.cleanup()
    print("stopped")
